#!/bin/sh

start_server()
{
  echo "[EULA] Accepting EULA"
  echo "#By changing the setting below to TRUE you are indicating your agreement to our EULA \(https://account.mojang.com/documents/minecraft_eula\)." > $SPIGOT_DATA/eula.txt
  echo "#Wed Feb 22 20:06:20 UTC 2017" >> $SPIGOT_DATA/eula.txt
  echo "eula=true" >> $SPIGOT_DATA/eula.txt

  echo "[Minecraft] Starting Minecraft"
  java -Xmx1G -jar ${SPIGOT_BIN}
}

start_server
